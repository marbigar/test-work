/* ----------------------------------------------------------- */
/*  MENU
/* ----------------------------------------------------------- */

$('.menu-main-hamburger').click(function() {
    $(this).toggleClass('active');
    $('.menu-main').toggleClass('active');
});

/**
 * If you click not on the menu and not on the hamburger - hide menu
 */
$(document).click(function(event) {
    var target = event.target;
    var isMenuClicked = $(target).closest('.menu-main').length;
    var isMenuButtonClicked = $(target).closest('.menu-main-hamburger').length;

    if (!isMenuClicked && !isMenuButtonClicked) {
        $('.menu-main-hamburger').removeClass('active');
        $('.menu-main').removeClass('active');
    }
});

/* ----------------------------------------------------------- */
/*  USER
/* ----------------------------------------------------------- */

$('.user').click(function() {
    $(this).toggleClass('active');
    $('.login-links').toggleClass('active');
});

/**
 * If you click not on the icon and not on the icon - hide login-links
 */
$(document).click(function(event) {
    var target = event.target;
    var isMenuClicked = $(target).closest('.login-links').length;
    var isMenuButtonClicked = $(target).closest('.user').length;

    if (!isMenuClicked && !isMenuButtonClicked) {
        $('.user').removeClass('active');
        $('.login-links').removeClass('active');
    }
});